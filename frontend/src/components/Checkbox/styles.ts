import styled from "styled-components";

import { colors } from "../../themes/colors";

export const CheckboxWrapper = styled.div`
  display: flex;
  margin-right: 1rem;
`;

export const CheckboxInput = styled.input`
  z-index: 500;
  padding: 0;
  margin: 0;
  width: 1.5rem;
  height: 1.5rem;
  opacity: 0;
  cursor: pointer;
`;

export const StyledCheckbox = styled.div<{ selected: boolean }>`
  display: flex;
  position: absolute;
  width: 1.5rem;
  height: 1.5rem;
  border: 4px solid ${colors.accent};
  border-radius: 6px;
  cursor: pointer;
  background: ${(props) => (props.selected ? colors.accent : "none")};
`;
