import React from "react";

import { CheckboxWrapper, CheckboxInput, StyledCheckbox } from "./styles";

interface Props {
  handleChange: any;
  select: boolean;
}

const Checkbox = ({ handleChange, select }: Props) => {
  return (
    <CheckboxWrapper>
      <CheckboxInput type="checkbox" onChange={handleChange} />
      <StyledCheckbox selected={select} />
    </CheckboxWrapper>
  );
};

export default Checkbox;
