import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconProp } from "@fortawesome/fontawesome-svg-core";

import { InputWrapper, InputIcon, StyledInput } from "./styles";

interface Props {
  type: string;
  placeholder?: string;
  icon: IconProp;
  handleChange: React.ChangeEventHandler;
}

const Input = ({ type, placeholder, icon, handleChange }: Props) => {
  return (
    <InputWrapper>
      <InputIcon>
        <FontAwesomeIcon icon={icon} />
      </InputIcon>
      <StyledInput
        type={type}
        placeholder={placeholder}
        onChange={handleChange}
      />
    </InputWrapper>
  );
};

export default Input;
