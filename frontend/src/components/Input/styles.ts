import styled from "styled-components";

import { colors } from "../../themes/colors";

export const InputWrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin: 1rem 0;
  padding: 0.5rem;
  border-bottom: 4px solid ${colors.accent};
`;

export const InputIcon = styled.div`
  display: flex;
  padding: 0.5rem;
`;

export const StyledInput = styled.input`
  display: flex;
  background: none;
  border: none;
  outline: none;
`;
