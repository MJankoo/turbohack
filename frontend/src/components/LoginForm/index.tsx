import React, { useState } from "react";
import { Link } from "react-router-dom";

import {
  LoginWrapper,
  Header,
  RegisterLinkAndButtonWrapper,
  RegisterLinkWrapper,
  RegisterLinkHeader,
  RegisterLink,
} from "./styles";

import Input from "../Input";
import Button from "../Button";

const LoginForm = () => {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const onFormSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    // console.log(email + " " + password);
  };

  const handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(e.target.value);
  };

  return (
    <LoginWrapper onSubmit={onFormSubmit}>
      <Header>Witaj ponownie!</Header>
      <Input
        type="email"
        placeholder="Email"
        icon="envelope"
        handleChange={handleEmailChange}
      />
      <Input
        type="password"
        placeholder="Password"
        icon="lock"
        handleChange={handlePasswordChange}
      />
      <RegisterLinkAndButtonWrapper>
        <RegisterLinkWrapper>
          <RegisterLinkHeader>Nie masz jeszcze konta?</RegisterLinkHeader>
          <Link to="/register">
            <RegisterLink>Zarejestruj się!</RegisterLink>
          </Link>
        </RegisterLinkWrapper>

        <Button>Zaloguj się!</Button>
      </RegisterLinkAndButtonWrapper>
    </LoginWrapper>
  );
};

export default LoginForm;
