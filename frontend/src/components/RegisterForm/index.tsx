import React, { useState, FormEvent, ChangeEvent } from "react";
import { Link } from "react-router-dom";

import {
  RegisterWrapper,
  Header,
  CheckboxAndTOSWrapper,
  TOSLabel,
  LoginLinkAndButtonWrapper,
  LoginLinkWrapper,
  LoginLinkHeader,
  LoginLink,
} from "./styles";

import Input from "../Input";
import Checkbox from "../Checkbox";
import Button from "../Button";

const RegisterForm = () => {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [repeatPassword, setRepeatPassword] = useState<string>("");
  const [checkbox, setCheckbox] = useState<boolean>(false);

  const onFormSubmit = (e: FormEvent) => {
    e.preventDefault();

    // console.log(email + " " + password + " " + repeatPassword + " " + checkbox);
  };

  const handleEmailChange = (e: ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e: ChangeEvent<HTMLInputElement>) => {
    setPassword(e.target.value);
  };

  const handleRepeatPasswordChange = (e: ChangeEvent<HTMLInputElement>) => {
    setRepeatPassword(e.target.value);
  };

  const handleCheckboxChange = (e: ChangeEvent<HTMLInputElement>) => {
    setCheckbox(!checkbox);
  };

  return (
    <RegisterWrapper onSubmit={onFormSubmit}>
      <Header>Zarejestruj się!</Header>
      <Input
        type="email"
        placeholder="Email"
        icon="envelope"
        handleChange={handleEmailChange}
      />
      <Input
        type="password"
        placeholder="Hasło"
        icon="lock"
        handleChange={handlePasswordChange}
      />
      <Input
        type="password"
        placeholder="Powtórz hasło"
        icon="lock"
        handleChange={handleRepeatPasswordChange}
      />

      <CheckboxAndTOSWrapper>
        <Checkbox select={checkbox} handleChange={handleCheckboxChange} />
        <TOSLabel>Oświadczam, że akceptuję warunki serwisu.</TOSLabel>
      </CheckboxAndTOSWrapper>

      <LoginLinkAndButtonWrapper>
        <LoginLinkWrapper>
          <LoginLinkHeader>Masz już konto?</LoginLinkHeader>
          <Link to="/login">
            <LoginLink>Zaloguj się!</LoginLink>
          </Link>
        </LoginLinkWrapper>

        <Button>Zarejestruj!</Button>
      </LoginLinkAndButtonWrapper>
    </RegisterWrapper>
  );
};

export default RegisterForm;
