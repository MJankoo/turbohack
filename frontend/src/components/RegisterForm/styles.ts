import styled from "styled-components";
import { colors } from "../../themes/colors";

export const RegisterWrapper = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-self: center;
  padding: 2rem 4rem;
  background: ${colors.beige};
  border-radius: 20px;
  box-shadow: ${colors.boxShadow};
`;

export const Header = styled.h1`
  display: flex;
  flex-direction: row;
  justify-content: center;
  font-family: "Catamaran", sans-serif;
  font-weight: 600;
`;

export const CheckboxAndTOSWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 1rem 0;
`;

export const TOSLabel = styled.div`
  display: flex;
  flex-direction: column;
  font-family: "Catamaran", sans-serif;
  font-size: 0.7rem;
  font-weight: 600;
  line-height: 1.2rem;
`;

export const LoginLinkAndButtonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 1rem 0;
`;

export const LoginLinkWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const LoginLinkHeader = styled.div`
  display: flex;
  justify-content: center;
  font-family: "Catamaran", sans-serif;
  font-size: 0.7rem;
  font-weight: 600;
`;

export const LoginLink = styled.span`
  display: flex;
  justify-content: center;
  margin: 0 1.5rem;
  font-family: "Catamaran", sans-serif;
  font-size: 0.7rem;
  font-weight: 600;
  line-height: 1.2rem;
  border-bottom: 4px solid ${colors.accent};
  cursor: pointer;

  &:hover {
    color: ${colors.textGray};
  }
`;
