import styled from "styled-components";

import { colors } from "../../themes/colors";

const Button = styled.button`
  display: flex;
  flex-direction: column;
  align-self: flex-end;
  margin: 1rem 0;
  padding: 0.8rem;
  color: ${colors.background};
  background: ${colors.accent};
  border-radius: 10px;
  border: none;
  cursor: pointer;
  font-family: "Catamaran", sans-serif;
  font-weight: 600;
  box-shadow: ${colors.boxShadow};

  &:hover {
    background: ${colors.accentSecondary};
  }
`;

export default Button;
