export const colors = {
  background: "#FFFFFF",
  beige: "#FFFCF6",
  accent: "#03E87A",
  accentSecondary: "#09FF88",
  text: "#000000",
  textGray: "#6b6b6b",
  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.15)",
};
