import React, { useState } from "react";
import { withRouter } from "react-router-dom";

import { RegisterWrapper } from "./styles";

import RegisterForm from "../../components/RegisterForm";

const Register = () => {
  return (
    <RegisterWrapper>
      <RegisterForm />
    </RegisterWrapper>
  );
};

export default withRouter(Register);
