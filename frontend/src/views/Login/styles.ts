import styled from "styled-components";

import { colors } from "../../themes/colors";
import wave from "../../assets/wave-1.svg";

export const LoginWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  min-height: 100vh;
  background-color: ${colors.background};
  background: url(${wave});
  background-size: cover;
  justify-content: center;
`;
