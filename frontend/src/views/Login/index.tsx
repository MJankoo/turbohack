import React from "react";
import { withRouter } from "react-router-dom";

import { LoginWrapper } from "./styles";

import LoginForm from "../../components/LoginForm";

const Login = () => {
  return (
    <LoginWrapper>
      <LoginForm />
    </LoginWrapper>
  );
};

export default withRouter(Login);
