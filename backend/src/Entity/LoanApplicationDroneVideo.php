<?php

namespace App\Entity;

use App\Repository\LoanApplicationDroneVideoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LoanApplicationDroneVideoRepository::class)
 */
class LoanApplicationDroneVideo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=LoanApplication::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $applicationId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $filePath;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getApplicationId(): ?LoanApplication
    {
        return $this->applicationId;
    }

    public function setApplicationId(LoanApplication $applicationId): self
    {
        $this->applicationId = $applicationId;

        return $this;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    public function setFilePath(string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }
}
