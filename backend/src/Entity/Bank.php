<?php

namespace App\Entity;

use App\Repository\BankRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BankRepository::class)
 */
class Bank
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $nip;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $logo;

    /**
     * @ORM\OneToMany(targetEntity=BankOffer::class, mappedBy="bank")
     */
    private $bankOffers;

    /**
     * @ORM\OneToMany(targetEntity=LoanApplication::class, mappedBy="bank")
     */
    private $loanApplications;

    public function __construct()
    {
        $this->bankOffers = new ArrayCollection();
        $this->loanApplications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNip(): ?string
    {
        return $this->nip;
    }

    public function setNIP(string $nip): self
    {
        $this->nip = $nip;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * @return Collection|BankOffer[]
     */
    public function getBankOffers(): Collection
    {
        return $this->bankOffers;
    }

    public function addBankOffer(BankOffer $bankOffer): self
    {
        if (!$this->bankOffers->contains($bankOffer)) {
            $this->bankOffers[] = $bankOffer;
            $bankOffer->setBank($this);
        }

        return $this;
    }

    public function removeBankOffer(BankOffer $bankOffer): self
    {
        if ($this->bankOffers->removeElement($bankOffer)) {
            // set the owning side to null (unless already changed)
            if ($bankOffer->getBank() === $this) {
                $bankOffer->setBank(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|LoanApplication[]
     */
    public function getLoanApplications(): Collection
    {
        return $this->loanApplications;
    }

    public function addLoanApplication(LoanApplication $loanApplication): self
    {
        if (!$this->loanApplications->contains($loanApplication)) {
            $this->loanApplications[] = $loanApplication;
            $loanApplication->setBank($this);
        }

        return $this;
    }

    public function removeLoanApplication(LoanApplication $loanApplication): self
    {
        if ($this->loanApplications->removeElement($loanApplication)) {
            // set the owning side to null (unless already changed)
            if ($loanApplication->getBank() === $this) {
                $loanApplication->setBank(null);
            }
        }

        return $this;
    }
}
