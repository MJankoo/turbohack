<?php

namespace App\Entity;

use App\Repository\BankOfferRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BankOfferRepository::class)
 */
class BankOffer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $interest;

    /**
     * @ORM\ManyToOne(targetEntity=Bank::class, inversedBy="bankOffers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bank;

    /**
     * @ORM\Column(type="json")
     */
    private $allowedFormOfEmployment = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private $allowedObligations;

    /**
     * @ORM\Column(type="smallint")
     */
    private $minimalOwnContribution;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInterest(): ?int
    {
        return $this->interest;
    }

    public function setInterest(int $interest): self
    {
        $this->interest = $interest;

        return $this;
    }

    public function getBank(): ?Bank
    {
        return $this->bank;
    }

    public function setBank(?Bank $bank): self
    {
        $this->bank = $bank;

        return $this;
    }

    public function getAllowedFormOfEmployment(): ?array
    {
        return $this->allowedFormOfEmployment;
    }

    public function setAllowedFormOfEmployment(?array $allowedFormOfEmployment): self
    {
        $this->allowedFormOfEmployment = $allowedFormOfEmployment;

        return $this;
    }

    public function getAllowedObligations(): ?bool
    {
        return $this->allowedObligations;
    }

    public function setAllowedObligations(bool $allowedObligations): self
    {
        $this->allowedObligations = $allowedObligations;

        return $this;
    }

    public function getMinimalOwnContribution(): ?int
    {
        return $this->minimalOwnContribution;
    }

    public function setMinimalOwnContribution(int $minimalOwnContribution): self
    {
        $this->minimalOwnContribution = $minimalOwnContribution;

        return $this;
    }
}
