<?php

namespace App\Entity;

use App\Repository\LoanApplicationVideoControllerRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LoanApplicationVideoControllerRepository::class)
 */
class LoanApplicationVideoController
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}
