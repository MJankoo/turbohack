<?php

namespace App\Entity;

use App\Repository\LoanApplicationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LoanApplicationRepository::class)
 */
class LoanApplication
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=11)
     */
    private $pesel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\Column(type="smallint")
     */
    private $ownContribution;

    /**
     * @ORM\Column(type="smallint")
     */
    private $period;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\Column(type="smallint")
     */
    private $numberOfChildren;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $maritalStatus;

    /**
     * @ORM\Column(type="smallint")
     */
    private $earnings;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $formOfEmployment;

    /**
     * @ORM\Column(type="boolean")
     */
    private $otherObligations;

    /**
     * @ORM\Column(type="integer")
     */
    private $monthlyExpenses;

    /**
     * @ORM\Column(type="date")
     */
    private $birthday;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity=Bank::class, inversedBy="loanApplications")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bank;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getNumberOfChildren(): ?int
    {
        return $this->numberOfChildren;
    }

    public function setNumberOfChildren(int $numberOfChildren): self
    {
        $this->numberOfChildren = $numberOfChildren;

        return $this;
    }

    public function getMaritalStatus(): ?string
    {
        return $this->maritalStatus;
    }

    public function setMaritalStatus(string $maritalStatus): self
    {
        $this->maritalStatus = $maritalStatus;

        return $this;
    }

    public function getEarnings(): ?int
    {
        return $this->earnings;
    }

    public function setEarnings(int $earnings): self
    {
        $this->earnings = $earnings;

        return $this;
    }

    public function getFormOfEmployment(): ?string
    {
        return $this->formOfEmployment;
    }

    public function setFormOfEmployment(string $formOfEmployment): self
    {
        $this->formOfEmployment = $formOfEmployment;

        return $this;
    }

    public function getOtherObligations(): ?bool
    {
        return $this->otherObligations;
    }

    public function setOtherObligations(bool $otherObligations): self
    {
        $this->otherObligations = $otherObligations;

        return $this;
    }

    public function getOwnContribution(): ?int
    {
        return $this->ownContribution;
    }

    public function setOwnContribution(int $ownContribution): self
    {
        $this->ownContribution = $ownContribution;

        return $this;
    }

    public function getPeriod(): ?int
    {
        return $this->period;
    }

    public function setPeriod(int $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getPesel(): ?string
    {
        return $this->pesel;
    }

    public function setPesel(string $pesel): self
    {
        $this->pesel = $pesel;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getMonthlyExpenses(): ?int
    {
        return $this->monthlyExpenses;
    }

    public function setMonthlyExpenses(int $monthlyExpenses): self
    {
        $this->monthlyExpenses = $monthlyExpenses;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getBank(): ?Bank
    {
        return $this->bank;
    }

    public function setBank(?Bank $bank): self
    {
        $this->bank = $bank;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
