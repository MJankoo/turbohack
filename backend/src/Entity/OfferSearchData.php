<?php

namespace App\Entity;

use App\Repository\OfferSearchDataRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OfferSearchDataRepository::class)
 */
class OfferSearchData
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="bigint")
     */
    private $amount;

    /**
     * @ORM\Column(type="integer")
     */
    private $period;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfChildren;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $maritalStatus;

    /**
     * @ORM\Column(type="integer")
     */
    private $age;

    /**
     * @ORM\Column(type="integer")
     */
    private $earnings;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $formOfEmployment;

    /**
     * @ORM\Column(type="boolean")
     */
    private $otherObligations;

    /**
     * @ORM\Column(type="smallint")
     */
    private $ownContribution;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getPeriod(): ?int
    {
        return $this->period;
    }

    public function setPeriod(int $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getNumberOfChildren(): ?int
    {
        return $this->numberOfChildren;
    }

    public function setNumberOfChildren(int $numberOfChildren): self
    {
        $this->numberOfChildren = $numberOfChildren;

        return $this;
    }

    public function getMaritalStatus(): ?string
    {
        return $this->maritalStatus;
    }

    public function setMaritalStatus(string $maritalStatus): self
    {
        $this->maritalStatus = $maritalStatus;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getEarnings(): ?int
    {
        return $this->earnings;
    }

    public function setEarnings(int $earnings): self
    {
        $this->earnings = $earnings;

        return $this;
    }

    public function getFormOfEmployment(): ?string
    {
        return $this->formOfEmployment;
    }

    public function setFormOfEmployment(string $formOfEmployment): self
    {
        $this->formOfEmployment = $formOfEmployment;

        return $this;
    }

    public function getOtherObligations(): ?bool
    {
        return $this->otherObligations;
    }

    public function setOtherObligations(bool $otherObligations): self
    {
        $this->otherObligations = $otherObligations;

        return $this;
    }

    public function getOwnContribution(): ?int
    {
        return $this->ownContribution;
    }

    public function setOwnContribution(int $ownContribution): self
    {
        $this->ownContribution = $ownContribution;

        return $this;
    }
}
