<?php

namespace App\Service;

use App\Entity\BankOffer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Serializer;

class OfferSearcher {
    private $em;

    public function __construct($em) {
        $this->em = $em;
    }

    public function search(array $data) {

        $offerRepository = $this->em->getRepository(BankOffer::class);
        $offers = $offerRepository->findOfferByCriteria($data['formOfEmployment'], $data['otherObligations'], $data['ownContribution']);

        $encoder = new JsonEncoder();
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
        ];
        $normalizer = array(new DateTimeNormalizer(), new ObjectNormalizer(null, null, null, null, null, null, $defaultContext));
        $serializer = new Serializer($normalizer, [$encoder]);


        $amountOfInstallments = $data['period']/12;
        $creditAmount = $data['amount'];
        $offers = $serializer->normalize($offers);
        $offersCollection = [];
        foreach($offers as $offer) {
            $interest = $offer['interest']/100;
            $offer['installment'] = $creditAmount*$interest/($amountOfInstallments*(1-($amountOfInstallments/pow($amountOfInstallments+$interest, $data['period']))));
            array_push($offersCollection, $offer);
        }

        $serializedOffers = json_encode($offersCollection);

        return $serializedOffers;
    }
}