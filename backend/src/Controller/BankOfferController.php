<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Bank;
use App\Entity\BankOffer;
use App\Form\BankOfferFormType;

class BankOfferController extends AbstractController
{
    /**
     * @Route("/bank/offer/{$id}", name="get_bank_offer", methods="GET")
     */
    public function getOfferAction(int $id)
    {

    }

    /**
     * @Route("/bank/offer", name="add_bank_offer", methods="POST")
     */
    public function createOfferAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $bankOffer = new BankOffer();
        $form = $this->createForm(BankOfferFormType::class, $bankOffer);

        $em = $this->getDoctrine()->getManager();
        $bank = $em->getRepository(Bank::class)->find($data['bank']);
        if(!$bank) {
            throw new HttpException(400, "Organization with that id doesn't exist");
        }


        $form->submit($data);
        if(!$form->isSubmitted() || !$form->isValid()) {
            throw new HttpException(400, "Invalid data");
        }

        $em->persist($bankOffer);
        $em->flush();

        return new Response("Successfully added offer", 200);
    }

}
