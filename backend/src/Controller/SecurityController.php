<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\HttpException;

use App\Entity\User;
use App\Form\UserFormType;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login", methods="POST")
     */
    public function loginAction(Request $request, UserPasswordHasherInterface $userPasswordHasher) {
        $session = $request->getSession();
        //dump($session);
        if(!$session->get('logged')) {
            $data = json_decode($request->getContent(), true);

            $em = $this->getDoctrine()->getManager();
            $userRepository = $em->getRepository(User::class);

            $user = $userRepository->findBy(['email' => $data['email']]);
            if (isset($user) && !empty($user) && isset($user[0])) {
                if ($userPasswordHasher->isPasswordValid($user[0], $data['password'])) {
                    $encoders = [new XmlEncoder(), new JsonEncoder()];
                    $normalizers = [new ObjectNormalizer()];

                    $serializer = new Serializer($normalizers, $encoders);
                    $response = $serializer->serialize($user[0], 'json');

                    $session->set('logged', true);
                    $session->set('user', $user[0]);


                    return new Response($response, 200);
                }
            }

            throw new HttpException(401, "Unauthorized");
        } else {
            return new Response("Successfully logged by session(Here will be returned user instance)", 200);
        }
    }

    /**
     * @Route("/register", name="register", methods="POST")
     */
    public function registerAction(Request $request, UserPasswordHasherInterface $userPasswordHasher) {
        $data = json_decode($request->getContent(), true);

        $user = new User();
        $form = $this->createForm(UserFormType::class, $user);

        $data['password'] = $userPasswordHasher->hashPassword($user, $data['password']);
        $form->submit($data);

        dump(count($data['roles']));
        if($form->isValid() && count($data['roles']) === 1) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return new Response("Successfully created", 200);
        }

        throw new HttpException(400, "Invalid data");

    }

    /**
     * @Route("/logout", name="logout", methods="POST")
     */
    public function logoutAction(Request $request) {
        $session = $request->getSession();
        if(!empty($session->get('user'))) {
            $session->invalidate();

            return new Response("Successfully logged out", 200);
        }

        return new Response("You didn't log in", 200);
    }
}