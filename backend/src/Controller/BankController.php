<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Serializer;

use App\Entity\Bank;
use App\Form\BankFormType;


class BankController extends AbstractController
{
    /**
     * @Route("/bank/{id}", name="get_bank", methods="GET")
     */
    public function getBankAcction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $bankRepository = $em->getRepository(Bank::class);

        $bank = $bankRepository->find($id);

        if(isset($bank) && !empty($bank)) {
            $encoder = new JsonEncoder();
            $defaultContext = [
                AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                    return $object->getId();
                },
            ];
            $normalizer = array(new DateTimeNormalizer(), new ObjectNormalizer(null, null, null, null, null, null, $defaultContext));

            $serializer = new Serializer($normalizer, [$encoder]);
            $serializedBank = $serializer->serialize($bank, 'json');

            return new Response($serializedBank , 200);
        }

        throw new HttpException(400, "Invalid data");
    }

    /**
     * @Route("/bank", name="add_bank", methods="POST")
     */
    public function createBankAction(Request $request)
    {
        $session = $request->getSession();
        $user = $session->get('user');
        if(!isset($user) || empty($user) || !in_array("ROLE_ADMIN", $session->get('user')->getRoles())) {
            throw new HttpException(401, "You are not permitted to this access");
        }

        $data = json_decode($request->getContent(), true);

        $bank = new Bank();
        $form = $this->createForm(BankFormType::class, $bank);

        $form->submit($data);
        if(!$form->isSubmitted() || !$form->isValid()) {
            throw new HttpException(400, "Invalid data");
        }

        $em = $this->getDoctrine()->getManager();
        if($em->getRepository(Bank::class)->findBy(['nip' => $data['nip']])) {
            throw new HttpException(400, "Organization with that NIP is already exist");
        }

        $em->persist($bank);
        $em->flush();

        return new Response("Successfully added organization", 200);
    }


}
