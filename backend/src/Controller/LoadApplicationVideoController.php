<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoadApplicationVideoController extends AbstractController
{
    /**
     * @Route("/load/application/{id}/video", name="load_application_video", methods="")
     */
    public function getApplicationVideo()
    {

    }
}
