<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Log\Logger;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\OfferSearchData;
use App\Form\OfferSearchDataFormType;
use App\Service\OfferSearcher;

class OfferSearchController extends AbstractController
{
    /**
     * @Route("/offer/search", name="offer_search_data", methods="POST")
     */
    public function searchOffer(Request $request): Response
    {
        $session = $request->getSession();
        $user = $session->get('user');
        if(!isset($user) || empty($user) || !in_array("ROLE_USER", $session->get('user')->getRoles())) {
            throw new HttpException(401, "You are not permitted to this access");
        }

        $offerSearcher = new OfferSearcher($this->getDoctrine()->getManager());

        $data = json_decode($request->getContent(), true);
        $creditSearchData = $this->validateData($data);

        $this->collectData($creditSearchData);
        $response = $offerSearcher->search($data);

        return new Response($response, 200);
    }

    private function collectData($creditSearchData) {
        $em = $this->getDoctrine()->getManager();
        $em->persist($creditSearchData);
        $em->flush();
    }

    private function validateData($data) {
        $creditSearchData = new OfferSearchData();
        $form = $this->createForm(OfferSearchDataFormType::class, $creditSearchData);

        $form->submit($data);
        if(!$form->isSubmitted() || !$form->isValid()) {
            throw new HttpException(400, "Invalid data");
        }

        return $creditSearchData;
    }
}
