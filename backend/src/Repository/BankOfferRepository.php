<?php

namespace App\Repository;

use App\Entity\BankOffer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BankOffer|null find($id, $lockMode = null, $lockVersion = null)
 * @method BankOffer|null findOneBy(array $criteria, array $orderBy = null)
 * @method BankOffer[]    findAll()
 * @method BankOffer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BankOfferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BankOffer::class);
    }

    public function findOfferByCriteria($allowedFormOfEmployment, $allowedObligations, $minimalOwnContribution)
    {
        $qb = $this->createQueryBuilder('o');
        $qb->select('o')
            ->where('o.allowedFormOfEmployment LIKE :allowedFormOfEmployment AND o.allowedObligations = :allowedObligations AND o.minimalOwnContribution < :minimalOwnContribution')
            ->setParameter('allowedFormOfEmployment', '%"'.$allowedFormOfEmployment.'"%')
            ->setParameter('allowedObligations', $allowedObligations)
            ->setParameter('minimalOwnContribution', $minimalOwnContribution);

        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return BankOffer[] Returns an array of BankOffer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BankOffer
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
