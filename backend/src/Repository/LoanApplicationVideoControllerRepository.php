<?php

namespace App\Repository;

use App\Entity\LoanApplicationVideoController;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LoanApplicationVideoController|null find($id, $lockMode = null, $lockVersion = null)
 * @method LoanApplicationVideoController|null findOneBy(array $criteria, array $orderBy = null)
 * @method LoanApplicationVideoController[]    findAll()
 * @method LoanApplicationVideoController[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LoanApplicationVideoControllerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LoanApplicationVideoController::class);
    }

    // /**
    //  * @return LoanApplicationVideoController[] Returns an array of LoanApplicationVideoController objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LoanApplicationVideoController
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
