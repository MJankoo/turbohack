<?php

namespace App\Repository;

use App\Entity\CreditSearchData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CreditSearchData|null find($id, $lockMode = null, $lockVersion = null)
 * @method CreditSearchData|null findOneBy(array $criteria, array $orderBy = null)
 * @method CreditSearchData[]    findAll()
 * @method CreditSearchData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferSearchDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CreditSearchData::class);
    }

    // /**
    //  * @return CreditSearchData[] Returns an array of CreditSearchData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CreditSearchData
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
