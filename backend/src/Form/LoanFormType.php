<?php

namespace App\Form;

use App\Entity\LoanApplication;
use App\Entity\User;
use App\Entity\Bank;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LoanFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pesel')
            ->add('name')
            ->add('surname')
            ->add('ownContribution')
            ->add('period')
            ->add('amount')
            ->add('numberOfChildren')
            ->add('maritalStatus', ChoiceType::class, [
                'choices' => [
                    'single' => 'single',
                    'married' => 'married',
                    'divorced' => 'divorced',
                    'widower' => 'widower',
                ],
            ])
            ->add('earnings')
            ->add('formOfEmployment', ChoiceType::class, [
                'choices' => [
                    'contractOfEmployment-timely' => 'contractOfEmployment-timely',
                    'contractOfEmployment-indefinite' => 'contractOfEmployment-indefinite',
                    'contractOfMandate' => 'contractOfMandate',
                    'contractWork' => 'contractWork',
                    'selfEmployment' => 'selfEmployment',
                    'jobSearch' => 'jobSearch',
                ],
            ])
            ->add('otherObligations', ChoiceType::class, [
                'choices' => [
                    'true' => 1,
                    'false' => 0,
                ],
            ])
            ->add('monthlyExpenses')
            ->add('birthday', DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                ])
            ->add('user', EntityType::class, [
                    'class' => User::class]
            )
            ->add('bank', EntityType::class, [
                    'class' => Bank::class]
            )
            ->add('status')
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'newInvestment' => 'newInvestment',
                    'existingProperty' => 'existingProperty',
                ],
            ])
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LoanApplication::class,
            'csrf_protection' => false,
        ]);
    }
}
