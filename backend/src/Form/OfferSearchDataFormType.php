<?php

namespace App\Form;

use App\Entity\OfferSearchData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OfferSearchDataFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('amount', NumberType::class)
            ->add('period', NumberType::class)
            ->add('numberOfChildren', NumberType::class)
            ->add('maritalStatus', ChoiceType::class, [
                'choices' => [
                    'single' => 'single',
                    'married' => 'married',
                    'divorced' => 'divorced',
                    'widower' => 'widower',
                ],
            ])
            ->add('age', NumberType::class)
            ->add('earnings', NumberType::class)
            ->add('formOfEmployment', ChoiceType::class, [
                'choices' => [
                    'contractOfEmployment-timely' => 'contractOfEmployment-timely',
                    'contractOfEmployment-indefinite' => 'contractOfEmployment-indefinite',
                    'contractOfMandate' => 'contractOfMandate',
                    'contractWork' => 'contractWork',
                    'selfEmployment' => 'selfEmployment',
                    'jobSearch' => 'jobSearch',
                ],
            ])
            ->add('ownContribution', NumberType::class)
            ->add('otherObligations', ChoiceType::class, [
                'choices' => [
                    'true' => 1,
                    'false' => 0,
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OfferSearchData::class,
            'csrf_protection' => false,
        ]);
    }
}
