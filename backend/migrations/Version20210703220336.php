<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210703220336 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE loan_application_drone_video (id INT AUTO_INCREMENT NOT NULL, application_id_id INT NOT NULL, file_path VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_FEE3372E9CD0792D (application_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE loan_application_drone_video ADD CONSTRAINT FK_FEE3372E9CD0792D FOREIGN KEY (application_id_id) REFERENCES loan_application (id)');
        $this->addSql('ALTER TABLE bank_offer CHANGE allowed_form_of_employment allowed_form_of_employment JSON NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE loan_application_drone_video');
        $this->addSql('ALTER TABLE bank_offer CHANGE allowed_form_of_employment allowed_form_of_employment LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
