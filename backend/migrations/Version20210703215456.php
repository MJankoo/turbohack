<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210703215456 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bank_offer CHANGE allowed_form_of_employment allowed_form_of_employment JSON NOT NULL');
        $this->addSql('ALTER TABLE loan_application ADD type VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE loan_application ADD CONSTRAINT FK_9A828511C8FB41 FOREIGN KEY (bank_id) REFERENCES bank (id)');
        $this->addSql('CREATE INDEX IDX_9A828511C8FB41 ON loan_application (bank_id)');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bank_offer CHANGE allowed_form_of_employment allowed_form_of_employment LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
        $this->addSql('ALTER TABLE loan_application DROP FOREIGN KEY FK_9A828511C8FB41');
        $this->addSql('DROP INDEX IDX_9A828511C8FB41 ON loan_application');
        $this->addSql('ALTER TABLE loan_application DROP type');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
